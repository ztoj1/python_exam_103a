# -*- coding: utf-8 -*-
"""
Created on Sat Mar  4 17:30:15 2023

@author: MCempa
"""
import random
import time
list_sum_line=[]
list_sum_col=[]
time.sleep(3)
def generate_board():
    print("GENEROWANIE TABLICY - w każdym z 9 kwadratów są liczby od 1 do 9 bez powtórzeń ")  
    print(" ") 
    board = [random.sample(range(1,10),9) for y in range(9)]
    #print(board)

    for i in range(0,3,1):
        for j in range(0,7,3):
            print (f"{board[i][j:j+3]} | {board[i+3][j:j+3]} | {board[i+6][j:j+3]}")
        print("________________________________")
    print("*******************************")     
    return board
   
# załozono ze jezeli suma liczb w każdym wierszu i kolumnie jest równa 45 to tablica spelnia załozenia SUDOKU
def check_line(board):   
    print("SPRAWDZANIE POPRAWNOSCI W RZĘDZIE:")
    for i in range(0,3,1):
        for j in range(0,7,3):
            sum_line=sum(board[i][j:j+3]) + sum(board[i+3][j:j+3]) + sum(board[i+6][j:j+3])
            #print(sum_line)
            if sum_line==45:
                list_sum_line.append(1)
    if len(list_sum_line)<9:
        print("Warunek poprawnosci rzędu nie spełniony")
    else:
        print("Warunek poprawnosci rzędu spełniony")
    print(" ")
    print("*******************************")    
    time.sleep(3)

def check_col(tablica):   
    print("SPRAWDZENIE POPRAWNOSCI W KOLUMNIE: ")
    for i in range(0,7,3):
        for j in range(0,3,1):     
            sum_col=sum(tablica[i][x] for x in [j,j+3,j+6]) + sum(tablica[i+1][x]for x in [j,j+3,j+6]) + sum(tablica[i+2][x]for x in [j,j+3,j+6])
            #print(sum_col)
            if sum_col==45:
                list_sum_col.append(1)
    if len(list_sum_col)<9:
        print("Warunek poprawnosci kolumny nie spelniony")
    else:
        print("Warunek poprawnosci kolumny spelniony")
    print(" ")
    print("*******************************")   
    time.sleep(3)
        
def main():
    board=generate_board()
    check_line(board)
    check_col(board)
                
if __name__== "__main__":
	main()            
            
            